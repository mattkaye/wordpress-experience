# WordPress Experience
For the past 5 years, I've been a part time senior freelance developer for the creative shop [Afteractive](https://www.afteractive.com/about/). In this role  I build WordPress plugins, extend website capabilites via the WordPress API as well as tackle a variety of client challenges.

I also deal with server administration for clients which includes provisioning Linux servers to suit the particular client needs.

Below you will find list of the most recent and intersting projects we've launched:

### United Way of Greater Philadelphia and Southern New Jersey
[![](https://i.imgur.com/ZuRuqtY.jpg)](https://www.unitedforimpact.org/)

### City Of Winter Park Florida
[![](https://i.imgur.com/rGw9gjd.png)](https://cityofwinterpark.org/)

### United States Tennis Association Florida Section
[![](https://i.imgur.com/LkQfwh1.jpg)](https://www.ustaflorida.com/)

### Natural Lands Conservation
[![](https://i.imgur.com/o4EoMtu.png)](https://natlands.org/)

### Millbridge - North Carolina Home Builders
[![](https://i.imgur.com/anQSn4Z.jpg)](https://millbridge-nc.com)

### Firetainment - Custom Made Fire Pits
[![](https://i.imgur.com/N3AsQa5.jpg)](https://www.firetainment.com/)